#!/bin/bash
#
# build all of the media asset zip files
# -- version and file names from 'parameters' in various subdirectories
# -- generates ./out/* which corresponds to paths of input tree
#
# -- does not re-generate files if they already exist in ./out/
#    (so blow away ./out if you want to recreate everything)
# -- separate script to determine what needs to be uploaded to lorry
#
#set -x
VERBOSE=/bin/false
#VERBOSE=/bin/true

# choose your verbosity
#ZIPNOISE=""
#ZIPNOISE="--display-dots"
ZIPNOISE="--quiet"

## parameters file looks like:
## DIR=CtsMediaDecoderTestCases
## VERSION=2.0
## DEST="https://storage.googleapis.com/android_media/cts/tests/tests/media/decoder/${DIR}-${VERSION}.zip"

DIRS="cts external frameworks"
find ${DIRS} -type f -name parameters | while read parameters
do
    DIRS=
    VERSION=
    TOOL=zip
    UNPACK_IN_SUBDIRECTORY=true
    source ${parameters}

    if [ "${DIR}" = "" ] ; then
        echo Missing DIR from ${parameters}
        continue
    fi
    if [ "${VERSION}" = "" ] ; then
        echo Missing VERSION from ${parameters}
        continue
    fi
    case "${TOOL}" in
        "zip"|"tar.gz")
            ;;
        *)
            echo "Misconfigured TOOL value in ${parameters}
            continue
            ;;
    esac
    case "${UNPACK_IN_SUBDIRECTORY}" in
        "false"|"true")
            ;;
        *)
            echo "Misconfigured UNPACK_IN_SUBDIRECTORY value in ${parameters}
            continue
            ;;
    esac


    # extract the relevant subdirectory info
    p=`echo ${parameters} |sed -e 's/parameters$//'`

    case ${TOOL} in
        zip)
            case ${UNPACK_IN_SUBDIRECTORY} in
                true)
                    zipfile=out/${p}/${DIR}-${VERSION}.zip
                    if [ -f ${zipfile} ] ; then
                        ${VERBOSE} && echo already have ${zipfile}
                        continue
                    fi
                    mkdir -p out/${p}
                    fullzipfile=`pwd`/${zipfile}
                    (cd ${p} && \
                            rm -f ${DIR}-${VERSION} && \
                            ln -s ${DIR} ${DIR}-${VERSION} && \
                            echo creating ${fullzipfile} ... &&
                            zip ${ZIPNOISE} -r ${fullzipfile} ${DIR}-${VERSION} &&
                            echo ... done creating ${fullzipfile} &&
                            rm -f ${DIR}-${VERSION})
                    ;;
                *)
                    echo "zip doesn't support UNPACK_IN_SUBDIRECTORY=${UNPACK_IN_SUBDIRECTORY} from ${parameters}"
                    continue
                    ;;
            esac
            ;;
        tar.gz)
            case ${UNPACK_IN_SUBDIRECTORY} in
                false)
                    tarfile=out/${p}/${DIR}-${VERSION}.tar.gz
                    if [ -f ${tarfile} ] ; then
                        ${VERBOSE} && echo already have ${tarfile}
                        continue
                    fi
                    mkdir -p out/${p}
                    fulltarfile=`pwd`/${tarfile}
                    (cd ${p}/${DIR} && \
                            echo creating ${fulltarfile} ... &&
                            tar czf ${TARNOISE} ${fulltarfile} . &&
                            echo ... done creating ${fulltarfile})
                    ;;
                *)
                    echo "tar.gz doesn't support /UNPACK_IN_SUBDIRECTORY=${UNPACK_IN_SUBDIRECTORY} from ${parameters}"
                    continue
                    ;;
            esac
            ;;
    esac


done

echo all good
exit 0

