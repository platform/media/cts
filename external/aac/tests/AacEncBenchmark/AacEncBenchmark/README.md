# Files for AacDecBenchmark

This folder contains media files for Aac encoder benchmark. These clips were taken from big
buck bunny movies from Blender Foundation (https://peach.blender.org/). We are using the audio
tracks, the only change made here is to remove the video tracks using ffmpeg. The reference clip
taken is of 6 channel and 48kHz sample rate which makes it feasible to downscale channel count,
sample rates and test all possible combinations.

atest automatically downloads and copies these files to the device.

These media files can also be copied to Device Under Test before running the tests,
in which case atest uses the files present on the device.

Here are the instructions for copying these files manually to the device.

```
$ connect the device under test via ADB.
$ chmod a+x copy_media.sh
$ ./copy_media.sh
```

If there are multiple devices connected under adb, add -s serial option to ./copy_media.sh


### Big Buck Bunny details
--------------------------------------------------------------

Attribution: (c) copyright 2008, Blender Foundation / www.bigbuckbunny.org
License: Creative Commons Attribution 3.0 license.
Download link: https://peach.blender.org/download/

The reference clip is converted to have only audio stream dropping video stream and downscaled
further using ffmpeg (ffmpeg.org) to test more possible combinations.
