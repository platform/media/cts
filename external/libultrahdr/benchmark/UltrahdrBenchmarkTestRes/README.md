# Files for Ultrahdr Benchmark Test

This folder contains media files for benchmark test in libultrahdr. These test files are a snapshot of the test files from frameworks/base/tests/graphics/SilkFX/assets/gainmaps/ in the Android Project as of December 18, 2024. They are replicated here so they can be used for unit(benchmark) testing.

Source Link: https://android.googlesource.com/platform/frameworks/base/+/refs/heads/main/tests/graphics/SilkFX/assets/gainmaps/

Here are the instructions for copying these files manually to the device.

```
$ connect the device under test via ADB.
$ chmod a+x copy_media.sh
$ ./copy_media.sh
```

If there are multiple devices connected under adb, add -s serial option to ./copy_media.sh
