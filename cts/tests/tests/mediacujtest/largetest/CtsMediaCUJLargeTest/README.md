# Files for CtsMediaCUJLargeTest

This folder contains media files for CtsMediaCUJLargeTest.
CtsMediaCUJLargeTest is part for cts mediacujtest which does E2E CUJ testing. This test suites uses media3 exoplayer as a player.
CtsMediaCUJLargeTest currently test playback for 5 min and 30 min video clips and verifies its playback time.

cts-tradefed automatically downloads and copies these files to the device.

These media files can also be copied to Device Under Test before running the CTS,
in which case cts-tradefed uses the files present on the device.

Here are the instructions for copying these files manually to the device.

```
$ connect the device under test via ADB.
$ chmod a+x copy_media.sh
$ ./copy_media.sh
```

If there are multiple devices connected under adb, add -s serial option to ./copy_media.sh

### Big Buck Bunny details
--------------------------------------------------------------

The video clip, Big Buck Bunny is copyrighted 2008 by Blender Foundation / www.bigbuckbunny.org under Creative Commons Attribution 3.0 license.

The original clip from www.bigbuckbunny.org is converted to various formats using ffmpeg (ffmpeg.org).


### Tears of Steel
--------------------------------------------------------------
The video clip, Tears of Steel is copyrighted 2012 by Blender Foundation / www.tearsofsteel.org under Creative Commons Attribution 3.0 license.

The original clip from www.tearsofsteel.org is converted to various formats using ffmpeg (ffmpeg.org).


### ElephantsDream
--------------------------------------------------------------
The video clip, Elephants Dream is copyrighted 2006 by Blender Foundation / www.elephantsdream.org under Creative Commons Attribution license.

The original clip from www.elephantsdream.org is converted to various formats using ffmpeg (ffmpeg.org).
