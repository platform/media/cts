#!/bin/bash
#
# after running build-all.sh, which generates all of the current zip files, run
# this script to figure out which ones are not yet in Lorry.
#
# Current limitations:
# -- only checks the CTS assets, not the unit test assets
# -- only flags that the assets need to be uploaded, does not perform the upload.
#
#set -x

VERBOSE=/bin/false
#VERBOSE=/bin/true

# auto_upload: no/yes/ask
auto_upload=no

TMP=`mktemp /tmp/curl.XXXXXX`
trap "rm -f ${TMP}" 0

# parse some simple command line options
# -- ask/yes/no
if [ "$1" != "" ] ; then
    case "$1" in
        -no)
            auto_upload=no
            ;;
        -yes)
            auto_upload=yes
            ;;
        -ask)
            auto_upload=ask
            ;;
        *)
            echo "Unrecognized argument"
            echo "Usage: $0 [-no|-yes|-ask]"
            exit 1
            ;;
    esac
    echo Auto-upload behavior: ${auto_upload}
fi

find out -type f | grep -E '.tar.gz$|.zip$' | sort | while read path
do
    # figure out where it lives in lorry
    case ${path} in
        out/cts*)
            # lorry path
            url_path=`echo ${path} | sed -e 's,^out/,,'`
            # https://dl.google.com/android/xts/cts/tests/media/CtsMediaV2TestCases-3.4.zip
            url=https://dl.google.com/android/xts/${url_path}
            ;;
        out/frameworks*|out/external*)
            # lorry path
            url_path=`echo ${path} | sed -e 's,^out/,,'`
            # https://dl.google.com/android-unittest/media/external/tremolo/tests/VorbisDecoderRes-1.0.zip
            url=https://dl.google.com/android-unittest/media/${url_path}
            ;;
        *)
            ${VERBOSE} && echo I do not know how to handle ${path}
            continue
            ;;
    esac
    # figure out if the file is present in lorry
    curl --silent --fail --head --output ${TMP} ${url}
    exists=$?
    ${VERBOSE} && echo ${exists} for whether ${url} exists
    if [ ${exists} = 0 ] ; then
        continue
    fi
    echo ""
    echo "Needs uploading  ${path}"
    echo "           dest  ${url}"
    # {possibly} figure out if the contents are the same
    # indicate that we need to upload it
    case "${auto_upload}" in
        no)
            continue
            ;;
        yes)
            # fall through to uploading
            ;;
        ask)
            echo -n "Upload? "
            read answer < /dev/tty
            case ff"${answer}" in
                ff[Yy]*)
                    # fall through to uploading
                    ;;
                *)
                    echo "NOT uploading ${path}"
                    continue
                    ;;
            esac
            ;;
        *)
            echo "auto_upload set to bad value: ${auto_upload}"
            continue
            ;;
    esac
    #
    echo "Uploading ..."
    ./upload2lorry.sh -no-presence-check ${path} ${url}
    result=$?
    if [ ${result} != 0 ] ; then
        # NOT controlled be VERBOSE. we want noise any time this fails
        echo "... UPLOAD FAILED"
        exit 1
    fi

done
